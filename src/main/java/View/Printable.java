package View;

import Model.Exceptions.IllegalParametersException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

@FunctionalInterface
public interface Printable {

    void print() throws IllegalParametersException,
            IOException;
}