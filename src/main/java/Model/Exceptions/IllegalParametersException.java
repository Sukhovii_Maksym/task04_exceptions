package Model.Exceptions;

public class IllegalParametersException extends Exception {
    public IllegalParametersException() {
    }

    public IllegalParametersException(String message) {
        super("IllegalParametersException:"+message);
    }
}
