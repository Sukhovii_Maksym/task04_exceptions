package Model.Exceptions;

public class MyRuntimeException extends RuntimeException {
    public MyRuntimeException() {
    }

    public MyRuntimeException(String message) {
        super("MyRuntimeException:"+message);
    }
}
