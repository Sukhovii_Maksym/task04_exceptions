package Model;

public interface Operations {
    Double add(Double first, Double second);
    Double minus(Double first, Double second);
    Double multiply(Double first,Double second);
    Double divide(Double first,Double second);
}
