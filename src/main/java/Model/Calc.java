package Model;

public class Calc implements Operations {

    public Calc() {
    }

    public Double add(Double first, Double second) {
        return first + second;
    }

    public Double minus(Double first, Double second) {

        return first - second;
    }

    public Double multiply(Double first, Double second) {
        return first * second;
    }

    public Double divide(Double first, Double second) {
        return first / second;
    }
}